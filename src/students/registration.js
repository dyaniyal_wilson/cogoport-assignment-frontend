import React from 'react';
import { Form, Row, Button, Col, Container } from 'react-bootstrap'
import Toast from 'react-bootstrap/Toast'
import Spinner from 'react-bootstrap/Spinner'
import { Redirect } from 'react-router-dom';
import axios from 'axios';

class Registration extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      validated: false, 
      id: props.match.params.id,
      name: '',
      age: '',
      division: '',
      gender: '',
      showToast: false,
      loading: true
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.toggleShow = this.toggleShow.bind(this);
  }

  componentDidMount() {
    const id = this.state.id

    if (id) {
      axios({
        method: 'get',
        url: 'http://localhost:3000/students/' + id})
      .then(res => {
        console.log(res)
        if (res.data) {
          this.setState({...res.data, loading: false});
        } else {
          console.log('redirect')
          this.props.history.push("/listing");
        }
      }) 
    } else
      this.setState({loading: false});
  }

  toggleShow() {
    this.setState(prevState => ({
      showToast: !prevState.showToast,
    }))
  }

  handleSubmit(event) {
    const form = event.currentTarget;
    console.log(form.checkValidity());
    event.preventDefault();
    event.stopPropagation();

    if (form.checkValidity() === false) {

      this.setState({validated: true})
    } else {
      const { id, validated, showToast, loading, ...post_date } = this.state;
      const create_action = id ? false : true;   

      console.log('http://localhost:3000/' + (create_action ? 'students' : 'students/'+id))
      axios({
        method: create_action ? 'post' : 'put',
        url: 'http://localhost:3000/' + (create_action ? 'students' : 'students/'+id),
        data: {student: post_date}
      }).then(res => {
        if (create_action) {
          console.log('reset form') 
          this.setState({      
            name: '',
            age: '',
            division: '',
            gender: '',
            showToast: !this.state.showToast
          })
        } else
          this.setState({validated: false, showToast: !this.state.showToast})
      }) 
    }

  }

  handleChange(event) {
    console.log(event.currentTarget.name)
    const field = event.currentTarget.name;
    const value = event.currentTarget.value;

    this.setState({[field]: value})
  }



  render() {

    const { id, name, age, validated, showToast, division, gender, loading } = this.state
    console.log( this.state)

    return !loading ? (
      <>
        <Row aria-live="polite"
          aria-atomic="true"
          style={{
            position: 'relative',
            minHeight: '100px'
          }}>
          <Row style={{
              position: 'absolute',
              top: 20,
              right: 200,
            }}>
            <Col md={12}>
              <Toast show={showToast} onClose={this.toggleShow} animation={true} autohide>
                <Toast.Body>{id ? 'Updated' : 'Registrated '} Successfully!!! </Toast.Body>
              </Toast>
            </Col>
          </Row>
        </Row>
        {<Container fluid>
          <Row className="justify-content-md-center">
            <Col md="auto"> <h1>{ id ? 'Edit ' : 'Add ' }Student Registration</h1>
              <Form noValidate validated={validated} onSubmit={this.handleSubmit} >
                <Form.Row>
                  <Form.Group as={Col} >
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" title='Name' name='name' placeholder="Enter name" value={name} required onChange={this.handleChange} />
                    <Form.Control.Feedback type="invalid" >Please Enter Name</Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group as={Col}   >
                    <Form.Label>Age</Form.Label>
                    <Form.Control type="number" title='Age' name='age' placeholder="Enter Age" value={age} required onChange={this.handleChange} />
                    <Form.Control.Feedback type="invalid" >Please Enter Age</Form.Control.Feedback>
                  </Form.Group>
                </Form.Row>

                <Form.Row>
                  <Form.Group as={Col} >
                    <Form.Label>Division</Form.Label>
                    <Form.Control title='Division' as="select" name='division' required value={division} onChange={this.handleChange}>
                      <option value="" >Select Division</option>
                      <option>A</option>
                      <option>B</option>
                      <option>C</option>
                      <option>D</option>
                      <option>E</option>
                      <option>F</option>
                    </Form.Control>
                    <Form.Control.Feedback type="invalid" >Please Select Division</Form.Control.Feedback>
                  </Form.Group>
                </Form.Row>

                <Form.Row>
                  <Form.Group as={Col} >
                    <Form.Label>Gender</Form.Label>
                    <Form.Control as="select" title='Gender' name='gender' required value={gender} onChange={this.handleChange} >
                      <option value="" >Select Gender</option>
                      <option>Male</option>
                      <option>Female</option>
                    </Form.Control>
                    <Form.Control.Feedback type="invalid" >Please Select Division</Form.Control.Feedback>
                  </Form.Group>
                </Form.Row>

                <Button variant="primary" type="submit">
                  {id ? 'Update': 'Create'}
                </Button>
              </Form>

            </Col>

          </Row>
        </Container>}

      </>
    ) : (
      <Container fluid>
        <Row className="justify-content-md-center" style={{position: 'absolute', top: '50%', left: '50%'}}>
          <Col md="auto">
            <Spinner animation="border" variant="primary" size="lg" />
          </Col>
        </Row>
      </Container>      
    )
  }
}

export default Registration;