import React from 'react';
import {
    striped, bordered, hover, FormControl, Button, Col, Row, Container
  } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import Badge from 'react-bootstrap/Badge'
import axios from 'axios';
import InputGroup from 'react-bootstrap/InputGroup'
import { Link } from 'react-router-dom';


class Listing extends React.Component {

  constructor(props) {
    super(props);
    this.state = { students: [], sort_order: 'desc', search_by_id: '' };
    this.allStudents = this.allStudents.bind(this);
  }

  componentDidMount() {
    this.allStudents();
  }

  allStudents() {
    axios({
      method: 'get',
      url: 'http://localhost:3000/students'})
    .then(res => {
      this.setState({ students: res.data });
    })    
  }

  sortColumns(column)  {
    axios({
      method: 'get',
      url: 'http://localhost:3000/students',
      params: {["sort_by["+column+"]"]: this.state.sort_order} })
    .then(res => {
      console.log(res)
      this.setState({ 
        students: res.data, 
        sort_order: this.state.sort_order == 'asc' ? 'desc' : 'asc'  
      });
    })
  }

  deleteStudents(id) {
    axios({
      method: 'delete',
      url: 'http://localhost:3000/students/'+id
    }).then(res => {
      console.log(res)
      const filteredStudent = this.state.students.filter((student) => student.id !== id);
      this.setState({ 
        students: filteredStudent
      });
    })    
  }

  handleChange(e) {
    console.log(e.target.value)
    let search_value = e.target.value;
    if (!search_value) {
      this.allStudents();
    }
    this.setState({search_by_id: search_value})
    console.log(this.state)
  }

  searchByID() {
    let search_by_id = this.state.search_by_id
    if (search_by_id) {
      axios({
        method: 'get',
        url: 'http://localhost:3000/students',
        params: {"search_by[id]": search_by_id} })
      .then(res => {
        console.log(res)
        this.setState({ 
          students: res.data
        });
      })    
    }
  }

  render() {

    let students = this.state.students

    return (
      <>

      <Container fluid>
        <h1>Students listing!</h1>
        <Row>
          <Col>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <Button variant="secondary" onClick={() => { this.searchByID() }} >Search Student by ID</Button>
              </InputGroup.Prepend>
              <FormControl aria-describedby="basic-addon1" type="number" onChange={(e) => this.handleChange(e) } />
            </InputGroup>
          </Col>
          <Col>
            <Button variant="success" onClick={() => { this.props.history.push('/registration')}}>Add Students</Button>
          </Col>
        </Row>
      </Container>


      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th onClick={() => { this.sortColumns('id') }} >ID <i className="fas fa-sort"></i></th>
            <th onClick={() => { this.sortColumns('name') }}>Name <i className="fas fa-sort"></i></th>
            <th onClick={() => { this.sortColumns('age') }}>Age <i className="fas fa-sort"></i></th>
            <th onClick={() => { this.sortColumns('division') }}>Division <i className="fas fa-sort"></i></th>
            <th onClick={() => { this.sortColumns('gender') }}>Gender <i className="fas fa-sort"></i></th>
            <th> Action </th>
          </tr>
        </thead>
        <tbody>
        {
          students.map((student, id) => (               
            <tr>
              <td>{student.id}</td>
              <td>{student.name}</td>
              <td>{student.age}</td>
              <td>{student.division}</td>
              <td>{student.gender}</td>
              <td>  
                <Badge pill variant="danger" className="mr-3" onClick={() => {this.deleteStudents(student.id)} }>
                  Delete
                </Badge>
                <Badge pill variant="light">
                  <Link to={"/students/"+student.id}> Edit</Link>
                </Badge>
              </td>
            </tr>
          ))
        }
        </tbody>
      </Table>

      </>
    )
  }
}

export default Listing;