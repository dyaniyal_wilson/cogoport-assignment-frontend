import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import registration from './students/registration';
import listing from './students/listing';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/registration" component={registration} />
        <Route path="/listing" component={listing} />
        <Route path="/students/:id" component={registration} />
        
        <Route path='*' exact={true} component={listing} />
      </Switch>
    </BrowserRouter>

  );
}

export default App;
